<?php

namespace Drupal\unique_multivalue_field\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the UniqueInteger constraint.
 */
class UniqueMultivalueFieldConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    if (!$this->isUnique($items->getValue())) {
      $this->context->addViolation($constraint->notUnique, [
        '%value' => $items->getFieldDefinition()->getLabel()
      ]);
    }
  }

  /**
   * Checks if the passed field items are unique.
   */
  private function isUnique($values) {
    $result= [];
    foreach ($values as $value) {
      foreach ($value as $item) {
        if(is_numeric($item)){
          $result[] = $item;
        }
      }
    }

    return count($result) == count(array_unique($result));
  }

}
