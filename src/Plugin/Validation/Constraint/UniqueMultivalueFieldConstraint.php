<?php

namespace Drupal\unique_multivalue_field\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Plugin implementation of the 'UniqueMultivalueField'.
 *
 * @Constraint(
 *   id = "UniqueMultivalueField",
 *   label = @Translation("Unique multivalue field validator", context = "Validation"),
 *   type = "field"
 * )
 */
class UniqueMultivalueFieldConstraint extends Constraint {

  // The message that will be shown if the value is empty.
  public $isEmpty = '%value is empty.';

  // The message that will be shown if the value is not unique.
  public $notUnique = '%value has duplicate entries.';

}
